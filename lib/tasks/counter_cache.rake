desc 'Counter cache for sm_classes has many students'

task task_counter: :environment do
  SmClass.reset_column_information
  SmClass.pluck(:id).each do |id|
    SmClass.reset_counters id, :students
  end
end
