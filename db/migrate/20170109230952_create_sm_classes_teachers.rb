class CreateSmClassesTeachers < ActiveRecord::Migration[5.0]
  def change
    create_table :sm_classes_teachers do |t|
      t.references :teacher, foreign_key: true
      t.references :sm_class, foreign_key: true
    end
  end
end
