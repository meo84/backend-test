class CreateSmClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :sm_classes do |t|
      t.string :name
      t.references :teacher

      t.timestamps
    end
  end
end
