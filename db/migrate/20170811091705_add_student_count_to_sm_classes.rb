class AddStudentCountToSmClasses < ActiveRecord::Migration[5.1]
  def change
    add_column :sm_classes, :student_count, :integer, default: 0
  end
end
