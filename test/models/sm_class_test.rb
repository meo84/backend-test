require 'test_helper'

class SmClassTest < ActiveSupport::TestCase
  describe '#student_count' do
    it 'returns 0 when sm_class is empty' do
      assert_equal 0, sm_classes(:jazz).student_count
    end

    it 'returns 1 when sm_class has one student' do
      sm_classes(:jazz).students << students(:bob)
      assert_equal 1, sm_classes(:jazz).student_count
    end
  end
end
