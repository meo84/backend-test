require 'test_helper'

class SmClassesControllerTest < ActionDispatch::IntegrationTest
  describe '#index' do
    it 'returns correct json when one empty sm_class' do
      get '/sm_classes', as: :json
      assert_equal '[{"id":1,"name":"Jazz","student_count":0}]', response.body
    end

    it 'returns correct json when one sm_class with one student' do
      sm_classes(:jazz).students << students(:bob)
      get '/sm_classes', as: :json
      assert_equal '[{"id":1,"name":"Jazz","student_count":1}]', response.body
    end

    it 'caches sm_classes data' do
      Rails.cache.clear
      assert_nil Rails.cache.fetch('sm_classes')
      get '/sm_classes', as: :json
      assert_equal ActiveSupport::Cache::DalliStore, Rails.cache.class
      refute_nil Rails.cache.fetch('sm_classes')
    end
  end
end
