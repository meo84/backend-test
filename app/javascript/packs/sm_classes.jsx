// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react'
import ReactDOM from 'react-dom'

class SmClass extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="large-10 medium-10 columns">{this.props.name}</div>
        <div className="large-2 medium-2 columns"><span className="badge">{this.props.student_count}</span> students</div>
      </div>
    )
  }
}

SmClass.propTypes = {
  id: React.PropTypes.number,
  name: React.PropTypes.string,
  student_count: React.PropTypes.number
}

SmClass.defaultProps = {
  student_count: 'n/a'
};

class SmClasses extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      smClasses: []
    };
  }

  componentDidMount() {
    fetch('/sm_classes.json')
    .then( response => {
      return response.json()
    })
    .then( json => {
      this.setState( { smClasses: json })
    })
  }

  render() {
    return (
      <div>
      <h3>SmClasses</h3>
      <ul className="no-bullet">
      { this.state.smClasses.map((smClass, index) => { 
        return <li key={smClass.id}><SmClass key={`class_${smClass.id}`} {...smClass} /></li>
      })}
      </ul>
      </div>
    )
  }
}

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <SmClasses/>,
    document.body.appendChild(document.createElement('div')),
  )
})
