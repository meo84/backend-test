class SmClassesController < ApplicationController
  def index
    respond_to do |format|
      format.json do
        sm_classes = Rails.cache.fetch('sm_classes', expires_in: 5.minutes) {
          SmClass.all
        }
        render json: sm_classes
      end
      format.html do
        render
      end
    end
  end
end
