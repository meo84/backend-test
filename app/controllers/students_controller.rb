class StudentsController < ApplicationController
  before_action :find_sm_class

  def index
    respond_to do |format|
      format.json do
        render json: @sm_class.students
      end
    end
  end

  private

  def find_sm_class
    @sm_class = SmClass.find(params[:sm_class_id])
  end
end
