class Teacher < ApplicationRecord
  has_many :sm_classes_teachers
  has_many :sm_classes, through: :sm_classes_teachers
end
