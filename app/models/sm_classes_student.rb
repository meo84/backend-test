class SmClassesStudent < ApplicationRecord
  belongs_to :student
  belongs_to :sm_class, counter_cache: :student_count
end
