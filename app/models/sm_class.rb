class SmClass < ApplicationRecord
  has_many :sm_classes_students, dependent: :destroy
  has_many :sm_classes_teachers
  has_many :students, through: :sm_classes_students
  has_many :teachers, through: :sm_classes_teachers
end
