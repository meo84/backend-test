class Student < ApplicationRecord
  has_many :sm_classes_students, dependent: :destroy
  has_many :sm_classes, through: :sm_classes_students
end
