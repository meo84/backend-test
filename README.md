Backend Test
=====================

# Description

This repository contains a rails backend server that serve a JSON api to exposes the following objects:
- SmClass
- Teacher
- Student

Commit your work in a private repository (on github or bitbucket), don't forget to give read access to users hellvinz and julienXX to the newly created repository and send a mail when you are done.

You can do as many commits as you like in the each branch.

## Getting started

### Setup

install ruby 2.3.3, node and [yarn](https://yarnpkg.com/en/docs/install#mac-tab)

install gems
```
bundle --path=.bundle
```

install node_modules
```
yarn i
```

migrate db
```
bin/rails db:setup
```

### Serving application

```
bin/foreman start
```

### Running tests

Because there must be tests for every line of added code

```
bin/rake test
```

# Student counters

The page located in url /sm_classes is displaying a list of SmClass with the number of their Students

Implement student_count [attribute](https://github.com/rails-api/active_model_serializers/blob/master/docs/general/serializers.md#attribute) in SmClass serializer that returns the number of Student for this SmClass

Verify that it is working by browsing /sm_classes

Commit the result in branch "counter"

# Performance

* optimize the performance by adding caches (rails counter cache/ activemodel serializers / [http caching](http://api.rubyonrails.org/classes/ActionController/ConditionalGet.html)(don't forget cache invalidation mecanisms) knowing that it is acceptable that a single user can have a stale counter for 5 minutes:
    * a user loading the page for the first time might see outdated cached data but it must not be over 5 minutes old
    * a user refreshing the page might see outdated cached data but it must not be over 5 minutes old

Commit the result in branch "performance"

# Database Modeling

A teacher can assign one or many exercises to a SmClass: each Student of this SmClass will have to do the Exercise

An Exercise has a due_date.

The relation Exercise <-> Student can be in these states:
* no started (when the Student hasn't done any work)
* submitted (when the Student has submitted a work) 
* accepted (when the Teacher has accepted the submitted work)
* rejected (when the Teacher has rejected the submitted work)
* late (when the Student has not submitted work and the due date is in the past)

Add a submitted_count attribute in SmClass serializer to display the number of "submitted" exercises for each class

Add a running_count attribute in SmClass serializer to display the number of "running" exercises for each class. A "running" Exercise is an Exercise given to a SmClass whose due_date is not in the past.


Commit the result in branch "database_modeling"

ex:

Class "Toto" has 2 students "Bob", "Jane"

An exercise "Basics" is given to class Toto

Another exercise "Advanced" is given to class Toto

Bob has submitted work for the exercice "Basics" but not for exercise "Advanced"

Jane has not submitted any work for both exercises

In /sm-classes.json, we must have the class Toto serialized with
- 2 in student_count
- 1 in submitted_count
- 2 in running_count
